#!/usr/bin/python

import datetime
import MySQLdb as mdb
import sys
from subprocess import call
import time
import os

offSpeed=7
restartSpeed=5
start = datetime.time(5, 45, 0)
end = datetime.time(22, 0, 0)

def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end

# Get fountain parms and average windspeed
try:
    con = mdb.connect('localhost', 'root', 'estrrado', 'Chronos')
    
    cur = con.cursor()
    cur.execute("SELECT AVG(windspeed) FROM mainTable where logdatetime > date_sub(now(), interval 5 minute);")
    rows = cur.fetchall()

    for row in rows:
        speed=round(row[0],2)
    speed=9.2
    cur.execute("SELECT * FROM fountainParms limit 1;")
    rows = cur.fetchall()

    for row in rows:
        start=(datetime.datetime.min + row[2]).time()
        end=(datetime.datetime.min + row[3]).time()
        offSpeed=row[4]
        restartSpeed=row[5]

except mdb.Error, e:
  
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit(1)

finally:
    
    if con:
        con.close()

f = open('/home/rj/.fountainState', 'r')
fstate = int(f.read(1))
f.close();
if fstate is 1:
    print "The fountain is on."
else: 
    print "The fountain is off."

time.ctime() 
print "The fountain runs between",start.strftime('%l:%M%p'),"and",end.strftime('%l:%M%p'),"when the"
print "avg windspeed is below "+str(offSpeed)+" mph. The restart speed is "+str(restartSpeed)+" mph.\n"

if time_in_range(start, end, datetime.datetime.now().time()):
    print "It's", time.strftime('%l:%M%p %Z'), "- Time for fountain to be on."
    if speed > offSpeed:
        print "Avg. windspeed of", speed,"mph is above the max windspeed ("+str(offSpeed)+" mph)."
        if fstate is 1:
            print "Turning off the fountain"
            with open("/home/rj/.fountainState", "w") as text_file:
                text_file.write("{0}".format(0))
        else:
            print "Leaving the fountain off."
    elif fstate is 0 and speed<restartSpeed:
        print "Avg. windspeed of", speed,"mph is below",restartSpeed ,"mph. Turning on the fountain"
        with open("/home/rj/.fountainState", "w") as text_file:
            text_file.write("{0}".format(1))
     ##   os.system("echo \"#fountain\" | mail -s \"#fountain\" trigger@recipe.ifttt.com,tlc.chronos@gmail.com")
    elif fstate is 0 and speed>restartSpeed:
        print "Avg. windspeed of "+str(speed)+" mph is above the restart speed ("+str(restartSpeed)+" mph). Leaving the fountain off."
    elif fstate is 1 and speed < offSpeed:
        print "Avg. windspeed of",speed,"mph is below",offSpeed ,"mph. Keeping the fountain on."
      ##  os.system("echo \"#fountain\" | mail -s \"#fountain\" trigger@recipe.ifttt.com,tlc.chronos@gmail.com")
    else:
        print "Avg. windspeed of",speed,"mph. Doing nothing?"
else:
    print "It's", datetime.datetime.now().time(),"- Time for fountain to be sleeping."
    if fstate is 1:
        with open("/home/rj/.fountainState", "w") as text_file:
            text_file.write("{0}".format(0))


